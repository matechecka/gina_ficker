package Interfaces;

import base_service.Card;
import base_service.CardReader;
import base_service.Ordination;
import base_service.TaetigkeitsBereich;
import kse_service.ErgebnisKonsultation;
import kse_service.Konsultationsdaten;
import ressources.Arzt;

import java.util.ArrayList;
import java.util.List;

public interface IService {

    //Anmelden
    List<CardReader> getAllPossibleReaders(); //Alle CardReader mit AdminCards werden asugelesen
    boolean Authentifizieren(String PIN, CardReader reader); //User gibt PIN zur Anmeldung ein
    boolean Anmelden(String ordi_id, String taetig_id);
    List<TaetigkeitsBereich> getAllTaetigkeitsbereiche(Ordination o);
    List<Ordination> Ordinationen(); //Alle Ordinationen für CheckBox auslesen, um dann Ord vom User auswählen zu lassen
    void OrdWaehlen(String ord_id); //User wählt Ord
    void Taetwaehlen(String taetigkeits_id); //User wählt Tatigkeitsbereich
    Arzt Infoabrufen(); //Daten des Arztes werden an GUI gesendet
    void CloseDialog(); //Dialog schließen, wenn User sich abmeldet

    //Kartendaten
    Card getActiveAdminCard(); //Aktive AdminCard wird ausgelesen
    List<Card> getAllPossibleECARDs(); //Verfügbare e-cards werden ausgelesen
    Card PatCard(); //Karte des ausgewählten Patienten wird ausgelesen
    String[] getTestScenarios(); //Testszenarien werden ausgelesen
    void setTestScenario(); //Ausgewähltes Testszenario wird einer ecard zugewiesen

    //Konsulation erfassen/anzeigen
    ErgebnisKonsultation AddNewKonsultation(String svnr, String svt, String fachgebietscode,
                                            String behandlungscode, String behandlungsdatum);
                                            //Codes für FachgebietID und BehandlungsID sollen in der Oberfläche als Text stehen
                                            //Rückgabewert liefert alle Konsultationsdaten zur Anzeige
    Konsultationsdaten getKonsultationsDaten(String svnr); //Alle Konsultationen eines Patienten
    void KonsultationStornieren(Konsultationsdaten ks); //Konsultation mit den übergebenen Daten stornieren



}
