package Interfaces;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public abstract class IContainer extends JFrame
        implements ActionListener, WindowListener {

    int frame_h = 750;
    int frame_w = 1500;
    int divider = frame_w/10;

    public IContainer(){
        /** TODO:
         *  - set up components
         */

    }


    public void initFrame(){
        //Framelayout
        setLayout(new BorderLayout());
        setTitle("Test GUI");
        setSize(frame_w, frame_h);
        //setExtendedState(Frame.MAXIMIZED_BOTH);
        //SplitPanel

    }

    public void initMenu(){
        JMenuBar mbar = new JMenuBar();
        setJMenuBar(mbar);
        mbar.setVisible(true);

    }
    public void windowClosing(WindowEvent evt){
        System.exit(0);
    }

    @Override
    public void windowOpened(WindowEvent e) {    }



    @Override
    public void windowClosed(WindowEvent e) {    }

    @Override
    public void windowIconified(WindowEvent e) {    }

    @Override
    public void windowDeiconified(WindowEvent e) {    }

    @Override
    public void windowActivated(WindowEvent e) {    }

    @Override
    public void windowDeactivated(WindowEvent e) {    }

    @Override
    public void actionPerformed(ActionEvent e) {    }

}
