package Logik;

import Interfaces.IService;
import base_service.*;
import kse_service.ErgebnisKonsultation;
import kse_service.Konsultationsdaten;
import ressources.Arzt;

import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.util.*;

public class ServiceImpl implements IService {
    String dialogid = null;
    IBaseService client = null;
    List<CardReader> readers = new ArrayList<CardReader>();
    ProduktInfo pi = new ProduktInfo();
    VertragspartnerV2 auth = new VertragspartnerV2();
    List<Ordination> ordinations = new ArrayList<Ordination>();

    public ServiceImpl(){
        try {
            Map<String, String> map = new HashMap<>();
            map.put("KU", "Kurativ");
            map.put("EC", "E-Card Partner");
            map.put("ELGA", "ELGA-Partner");
            map.put("GU", "Vorsorgeuntersuchung");
            map.put("HAPO", "Hausapothekenvertrag");
            map.put("REZ", "Rezeptur");
            BaseServiceLocator locatorm = new BaseServiceLocator();
            client = locatorm.getbase_15();
            pi.setProduktId(1);
            pi.setProduktVersion("1");
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    @Override
    public List<CardReader> getAllPossibleReaders() {
        try {
            readers = Arrays.asList(client.getCardReaders());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return readers;
    }

    @Override
    public boolean Authentifizieren(String PIN, CardReader reader) {
        try {
            dialogid = client.createDialog(reader.getId(), pi, null, false );
            auth = client.authenticateDialog(dialogid,  null, PIN, reader.getId());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return true;
    }

    @Override
    public boolean Anmelden(String ordi_id, String taetig_id) {
        try {
            client.setDialogAddress(dialogid, ordi_id, taetig_id, null, null, null );
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<TaetigkeitsBereich> getAllTaetigkeitsbereiche(Ordination o) {
        return Arrays.asList(o.getTaetigkeitsBereich());
    }

    @Override
    public List<Ordination> Ordinationen() {
        ordinations = Arrays.asList(auth.getOrdination());
        return ordinations;
    }

    @Override
    public void OrdWaehlen(String ord_id) {

    }

    @Override
    public void Taetwaehlen(String taetigkeits_id) {

    }

    @Override
    public Arzt Infoabrufen() {
        return null;
    }

    @Override
    public void CloseDialog() {

    }

    @Override
    public Card getActiveAdminCard() {
        return null;
    }

    @Override
    public List<Card> getAllPossibleECARDs() {
        return null;
    }

    @Override
    public Card PatCard() {
        return null;
    }

    @Override
    public String[] getTestScenarios() {
        return new String[0];
    }

    @Override
    public void setTestScenario() {

    }

    @Override
    public ErgebnisKonsultation AddNewKonsultation(String svnr, String svt, String fachgebietscode, String behandlungscode, String behandlungsdatum) {
        return null;
    }

    @Override
    public Konsultationsdaten getKonsultationsDaten(String svnr) {
        return null;
    }

    @Override
    public void KonsultationStornieren(Konsultationsdaten ks) {

    }
}
