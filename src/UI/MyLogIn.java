package UI;


import Interfaces.IContainer;
import Logik.ServiceImpl;
import base_service.CardReader;
import base_service.Ordination;
import base_service.TaetigkeitsBereich;
import com.sun.corba.se.impl.ior.TaggedProfileTemplateFactoryFinderImpl;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class MyLogIn extends IContainer {

    ServiceImpl si = new ServiceImpl();
    CardReader reader = null;
    JComboBox<Ordination> comboBoxOrdination = new JComboBox<>();
    JComboBox<TaetigkeitsBereich> comboBoxTaetig = new JComboBox<>();

    public MyLogIn(CardReader reader) {
        super();
        initFrame();
        initMenu();
        setVisible(true);
        this.reader = reader;
    }

    @Override
    public void initFrame() {
        super.initFrame();
        this.setSize(800, 280);

        //Jpanel + FlowLayout + Gridlayout für linksbündig
        FlowLayout flowLayout = new FlowLayout();
        flowLayout.setAlignment(FlowLayout.LEFT);
        setLayout(flowLayout);
        GridLayout gridLayout = new GridLayout(0 ,3);
        gridLayout.setHgap(10);
        gridLayout.setVgap(10);
        final JPanel jPanel = new JPanel(gridLayout);
        add(jPanel);

        //jlable für PIN
        JLabel textforpin = new JLabel("PIN:");
        jPanel.add(textforpin);

        //PasswordBox
        JPasswordField passwordField = new JPasswordField(5);
        jPanel.add(passwordField);

        //Button zum anmelden
        JButton pinbutton = new JButton("OK");
        jPanel.add(pinbutton);







        //comboBoxOrdination Fachgebiete


        pinbutton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(si.Authentifizieren(new String(passwordField.getPassword()), reader)) {

                    //jlable für Adresse
                    JLabel textadresse = new JLabel("Ordinationen:");
                    jPanel.add(textadresse);
                    textadresse.setVisible(true);

                    //comboBoxOrdination Ordination
                    List<Ordination> ordinations = si.Ordinationen();

                    comboBoxOrdination = new JComboBox<Ordination>(ordinations.toArray(new Ordination[0]));
                    comboBoxOrdination.setRenderer(new DefaultListCellRenderer() {
                                             @Override
                                             public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                                                 Ordination o = (Ordination) value;
                                                 String anzeige = o.getPostleitzahl() + " " + o.getStrasse();

                                                 return super.getListCellRendererComponent(list, anzeige, index, isSelected, cellHasFocus);
                                             }
                                         }
                    );
                    jPanel.add(comboBoxOrdination);
                    jPanel.revalidate();

                    comboBoxOrdination.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            System.out.println("Actionlistener started");
                            JLabel texttaetig = new JLabel("Tateigkeitsbereiche:");
                            jPanel.add(texttaetig);

                            //comboBoxOrdination Tateig
                            List<TaetigkeitsBereich> taetigs = si.getAllTaetigkeitsbereiche((Ordination) comboBoxOrdination.getSelectedItem());

                            comboBoxTaetig = new JComboBox<TaetigkeitsBereich>(taetigs.toArray(new TaetigkeitsBereich[0]));
                            comboBoxTaetig.setRenderer(new DefaultListCellRenderer() {
                                                     @Override
                                                     public Component getListCellRendererComponent(JList<?> list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                                                         TaetigkeitsBereich t = (TaetigkeitsBereich) value;
                                                         String anzeige = t.getAnzeigeText();

                                                         return super.getListCellRendererComponent(list, anzeige, index, isSelected, cellHasFocus);
                                                     }
                                                 }
                            );
                            jPanel.add(comboBoxTaetig);
                            jPanel.revalidate();


                        }
                    });

                    comboBoxTaetig.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Ordination o = (Ordination) comboBoxOrdination.getSelectedItem();
                            String ord = o.getOrdinationId();

                            TaetigkeitsBereich t = (TaetigkeitsBereich) comboBoxTaetig.getSelectedItem();
                            String taet = t.getId();

                            si.Anmelden(ord, taet);
                            System.out.println("didi it fucker ");
                        }
                    });
                }

            }

        });

    }

    @Override
    public void initMenu() {
        super.initMenu();

    }
}