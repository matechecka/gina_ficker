package UI;

import Interfaces.IContainer;
import Logik.ServiceImpl;
import base_service.CardReader;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class MyContainer extends IContainer {

    private int frame_h = 750;
    private int frame_w = 1500;
    private int divider = frame_w/8;
    private String[] testitems = {"item one", "item two", "item three"};
    CardReader reader = null;
    ServiceImpl si = new ServiceImpl();

    public MyContainer(){
        super();
        initFrame();
        initMenu();
        setVisible(true);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }


    @Override
    public void initFrame() {
        super.initFrame();
        //SplitPane
        JSplitPane sp = new JSplitPane();
        sp.setDividerLocation(divider);
        JSplitPane sp1 = new JSplitPane();
        sp1.setDividerLocation(frame_w - 600);
        sp.setRightComponent(sp1);
        sp.setMinimumSize(
                new Dimension(frame_w/10, getHeight()));
        add(sp);

        //Left panel
        JPanel left = new JPanel
                (new GridLayout(1, 2, 5, 1));
        sp.add(left ,
                JSplitPane.LEFT);
        left.add(new JLabel("Patientenname"));
        left.add(new JList<>(new String[]{"KS one", "KS two", "KS three"}));

        //Middle panel
        JPanel middle = new JPanel(new BorderLayout());
        sp1.setLeftComponent(middle);
        JLabel north = new JLabel("<HTML><h1>Konsultationsdaten<h1/></HTML>");
        north.setHorizontalAlignment(SwingConstants.CENTER);
        middle.add(north, BorderLayout.NORTH);
        JLabel text = new JLabel("<HTML>this is the test text for consultation data <br/>" +
                                    "it contains several lines of text with paragraphs\n" +
                                    "and now there is enough text in it</HTML>");
        text.setHorizontalAlignment(SwingConstants.CENTER);
        middle.add(text, BorderLayout.CENTER);

        //Right panel
        JPanel right = new JPanel(new BorderLayout());
        sp1.setRightComponent(right);

        JSplitPane split_horizontal = new JSplitPane();
        split_horizontal.setOrientation(JSplitPane.VERTICAL_SPLIT);
        split_horizontal.setDividerLocation(220);
        right.add(split_horizontal);
        JPanel top = new JPanel(new BorderLayout());
        JPanel mid = new JPanel(new BorderLayout());
        JPanel down = new JPanel(new BorderLayout());
        split_horizontal.setLeftComponent(top);
        JSplitPane horizontal_down_split = new JSplitPane();
        horizontal_down_split.setOrientation(JSplitPane.VERTICAL_SPLIT);
        horizontal_down_split.setDividerLocation(220);
        split_horizontal.setRightComponent(horizontal_down_split);
        horizontal_down_split.setLeftComponent(mid);
        horizontal_down_split.setRightComponent(down);

        JLabel cardinfo = new JLabel("<HTML><h2>Info über gesteckte Karte</h2></HTML>");
        cardinfo.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel card_text = new JLabel("<HTML>this is the sample text which will be used to preset the place<br/> where the card info belongs to</HTML>");
        card_text.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel personinfo = new JLabel("<HTML><h2>Info über den Besitzer der obrigen Karte</h2></HTML>");
        personinfo.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel persontext = new JLabel("<HTML>don't give attention to me, I'm just cleaning here</HTML>");
        persontext.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel testscenarioinfo = new JLabel("<HTML><h2>Testszenarios</h2></HTML>");
        testscenarioinfo.setHorizontalAlignment(SwingConstants.CENTER);
        JLabel testscenariotext = new JLabel("<HTML>Hello you, I'm a placeholder! <br> I will be replaced with a list full of testscenarios," +
                                                    "are you looking forward to it?");
        testscenariotext.setHorizontalAlignment(0);
        top.add(cardinfo, BorderLayout.NORTH);
        top.add(card_text, BorderLayout.CENTER);
        mid.add(personinfo, BorderLayout.NORTH);
        mid.add(persontext, BorderLayout.CENTER);
        down.add(testscenarioinfo, BorderLayout.NORTH);
        down.add(testscenariotext, BorderLayout.CENTER);


    }

    @Override
    public void initMenu() {
        super.initMenu();
        super.getJMenuBar().setBackground(Color.white);
        super.getJMenuBar().setBorderPainted(true);
        //Anmeldefenster
        JMenu loginM = new JMenu("Anmelden");
        List<JMenuItem> menues = new ArrayList<JMenuItem>();
        List<CardReader> readers = si.getAllPossibleReaders();
        for(CardReader r : readers){
            JMenuItem ad_card = new JMenuItem(r.getId());
            menues.add(ad_card);
        }
        for(JMenuItem jm : menues){
            loginM.add(jm);
            jm.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    reader = readers.get(menues.indexOf(jm));
                    System.out.println("test");
                    MyLogIn ML = new MyLogIn(reader);
                }
            });
        }


        //Patientenmenü
        JMenu patM = new JMenu("Patient");
        JMenuItem kon_erf = new JMenuItem("Konsultation erfassen");
        JMenuItem alle_kon = new JMenuItem("Alle Konsultationen");
        patM.add(kon_erf);
        patM.add(alle_kon);

        //e-card Menü
        JMenu ecard = new JMenu("e-card");
        JMenuItem pat_card = new JMenuItem("card_one");
        ecard.add(pat_card);
        super.getJMenuBar().add(loginM);
        super.getJMenuBar().add(patM);
        super.getJMenuBar().add(ecard);

        kon_erf.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //System.out.println("test");
            }
        });

        alle_kon.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        pat_card.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

    }
}
