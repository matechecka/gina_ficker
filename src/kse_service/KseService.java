/**
 * KseService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package kse_service;

public interface KseService extends javax.xml.rpc.Service {
    public java.lang.String getkse_17Address();

    public kse_service.IKseService getkse_17() throws javax.xml.rpc.ServiceException;

    public kse_service.IKseService getkse_17(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
