/**
 * Kse_17BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package kse_service;

public class Kse_17BindingStub extends org.apache.axis.client.Stub implements kse_service.IKseService {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[25];
        _initOperationDesc1();
        _initOperationDesc2();
        _initOperationDesc3();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getErstkonsultationen");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "svNummer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "fachgebietsCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "nacherfassung"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "erstkonsultationsdaten"));
        oper.setReturnClass(kse_service.Erstkonsultationsdaten[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("aendernKonsultation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), java.lang.Long.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsVersion"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), java.lang.Integer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsfallCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationsBeleg"));
        oper.setReturnClass(kse_service.KonsultationsBeleg.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("checkStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "property"));
        oper.setReturnClass(kse_service.Property[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteKonsultationsdatenAnfrage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "anfrageId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[3] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteOfflineRecord");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "offlineId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[4] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("doKonsultation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "cin"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "svNummer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "svtCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "anspruchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "fachgebietsCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsfallCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "nacherfassungsgrundCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsdatum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "forceExecution"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "ergebnisKonsultation"));
        oper.setReturnClass(kse_service.ErgebnisKonsultation.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "kse_service.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[5] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("doKonsultationOffline");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "cin"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "svNummer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "svtCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "fachgebietsCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsfallCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "nacherfassungsgrundCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsdatum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "forceExecution"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "offlineRecord"));
        oper.setReturnClass(kse_service.OfflineRecord.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "kse_service.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[6] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("downloadKonsultationsdaten");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "anfrageId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationsdaten"));
        oper.setReturnClass(kse_service.Konsultationsdaten[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[7] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getBehandlungsfaelle");
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "baseProperty"));
        oper.setReturnClass(kse_service.BaseProperty[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        _operations[8] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getBehandlungsfaelleByFachgebiet");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "fachgebietCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "baseProperty"));
        oper.setReturnClass(kse_service.BaseProperty[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        _operations[9] = oper;

    }

    private static void _initOperationDesc2(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getBehandlungsfaelleByFachgebietZusatz");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "fachgebietCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsfallZusatzProperty"));
        oper.setReturnClass(kse_service.BehandlungsfallZusatzProperty[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        _operations[10] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getKonsultationsdaten");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "selektionsKriterien"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "suchFilter"), kse_service.SuchFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "selektionsart"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "sortierung"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationsdaten"));
        oper.setReturnClass(kse_service.Konsultationsdaten[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[11] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getKonsultationsdatenAnfragen");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "onlyReady"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationdatenAnfrage"));
        oper.setReturnClass(kse_service.KonsultationdatenAnfrage[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[12] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getLimit");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "zeitraum"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), java.lang.Integer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "limit"));
        oper.setReturnClass(kse_service.Limit[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[13] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getNumberOfflineKonsultationen");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        oper.setReturnClass(java.lang.Long.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[14] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getNumberOfflineNachsignaturen");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        oper.setReturnClass(java.lang.Long.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[15] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getOfflineRecords");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "offlineRecord"));
        oper.setReturnClass(kse_service.OfflineRecord[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[16] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("nachsignierenKonsultationen");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "cin"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "forceExecution"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationsBeleg"));
        oper.setReturnClass(kse_service.KonsultationsBeleg[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "kse_service.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[17] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("nachsignierenKonsultationenOffline");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "cin"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "forceExecution"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"), java.lang.Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "offlineRecord"));
        oper.setReturnClass(kse_service.OfflineRecord.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "kse_service.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[18] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendKonsultationsdatenAnfrage");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "selektionsKriterien"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "suchFilter"), kse_service.SuchFilter.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[19] = oper;

    }

    private static void _initOperationDesc3(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendOfflineKonsultation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "offlineId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "fachgebietsCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsfallCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "svtCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "anspruchId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "ergebnisKonsultation"));
        oper.setReturnClass(kse_service.ErgebnisKonsultation.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[20] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendOfflineNachsignatur");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "offlineId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationsBeleg"));
        oper.setReturnClass(kse_service.KonsultationsBeleg[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[21] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("sendZusatzinformationsAntwort");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "zusatzinformationsAntwortId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), java.lang.Long.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "zusatzinformationsAntwort"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), java.lang.Integer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[22] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("stornierenKonsultation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), java.lang.Long.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsVersion"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), java.lang.Integer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationsBeleg"));
        oper.setReturnClass(kse_service.KonsultationsBeleg.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[23] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("wiederinkraftsetzenKonsultation");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"), java.lang.Long.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsVersion"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"), java.lang.Integer.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "ergebnisKonsultation"));
        oper.setReturnClass(kse_service.ErgebnisKonsultation.class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "InvalidParameterException"),
                      "kse_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "KseException"),
                      "kse_service.KseExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "kse_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "kse_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "kse_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[24] = oper;

    }

    public Kse_17BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public Kse_17BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public Kse_17BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.1");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent");
            cachedSerQNames.add(qName);
            cls = kse_service.AccessExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "baseExceptionContent");
            cachedSerQNames.add(qName);
            cls = kse_service.BaseExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent");
            cachedSerQNames.add(qName);
            cls = kse_service.CardExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent");
            cachedSerQNames.add(qName);
            cls = kse_service.DialogExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent");
            cachedSerQNames.add(qName);
            cls = kse_service.ServiceExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "invalidParameterExceptionContent");
            cachedSerQNames.add(qName);
            cls = kse_service.InvalidParameterExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent");
            cachedSerQNames.add(qName);
            cls = kse_service.KseExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "baseProperty");
            cachedSerQNames.add(qName);
            cls = kse_service.BaseProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "property");
            cachedSerQNames.add(qName);
            cls = kse_service.Property.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "svPersonV2");
            cachedSerQNames.add(qName);
            cls = kse_service.SvPersonV2.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "anspruch");
            cachedSerQNames.add(qName);
            cls = kse_service.Anspruch.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsfallZusatzProperty");
            cachedSerQNames.add(qName);
            cls = kse_service.BehandlungsfallZusatzProperty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "ergebnisKonsultation");
            cachedSerQNames.add(qName);
            cls = kse_service.ErgebnisKonsultation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "erstkonsultationsdaten");
            cachedSerQNames.add(qName);
            cls = kse_service.Erstkonsultationsdaten.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationdatenAnfrage");
            cachedSerQNames.add(qName);
            cls = kse_service.KonsultationdatenAnfrage.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationsBeleg");
            cachedSerQNames.add(qName);
            cls = kse_service.KonsultationsBeleg.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationsdaten");
            cachedSerQNames.add(qName);
            cls = kse_service.Konsultationsdaten.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "limit");
            cachedSerQNames.add(qName);
            cls = kse_service.Limit.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "limitKonfig");
            cachedSerQNames.add(qName);
            cls = kse_service.LimitKonfig.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "limitValue");
            cachedSerQNames.add(qName);
            cls = kse_service.LimitValue.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "offlineKonsultation");
            cachedSerQNames.add(qName);
            cls = kse_service.OfflineKonsultation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "offlineRecord");
            cachedSerQNames.add(qName);
            cls = kse_service.OfflineRecord.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "suchFilter");
            cachedSerQNames.add(qName);
            cls = kse_service.SuchFilter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "zusatzinformation");
            cachedSerQNames.add(qName);
            cls = kse_service.Zusatzinformation.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public kse_service.Erstkonsultationsdaten[] getErstkonsultationen(java.lang.String dialogId, java.lang.String svNummer, java.lang.String fachgebietsCode, java.lang.Boolean nacherfassung) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getErstkonsultationen"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, svNummer, fachgebietsCode, nacherfassung});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.Erstkonsultationsdaten[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.Erstkonsultationsdaten[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.Erstkonsultationsdaten[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.KonsultationsBeleg aendernKonsultation(java.lang.String dialogId, java.lang.Long konsId, java.lang.Integer konsVersion, java.lang.String behandlungsfallCode) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "aendernKonsultation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, konsId, konsVersion, behandlungsfallCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.KonsultationsBeleg) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.KonsultationsBeleg) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.KonsultationsBeleg.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.Property[] checkStatus(java.lang.String dialogId) throws java.rmi.RemoteException, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "checkStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.Property[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.Property[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.Property[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void deleteKonsultationsdatenAnfrage(java.lang.String dialogId, java.lang.String anfrageId) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "deleteKonsultationsdatenAnfrage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, anfrageId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void deleteOfflineRecord(java.lang.String dialogId, java.lang.String offlineId) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[4]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "deleteOfflineRecord"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, offlineId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.ErgebnisKonsultation doKonsultation(java.lang.String cin, java.lang.String dialogId, java.lang.String svNummer, java.lang.String svtCode, java.lang.String anspruchId, java.lang.String fachgebietsCode, java.lang.String behandlungsfallCode, java.lang.String nacherfassungsgrundCode, java.lang.String behandlungsdatum, java.lang.Boolean forceExecution, java.lang.String cardReaderId) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent, kse_service.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[5]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "doKonsultation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cin, dialogId, svNummer, svtCode, anspruchId, fachgebietsCode, behandlungsfallCode, nacherfassungsgrundCode, behandlungsdatum, forceExecution, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.ErgebnisKonsultation) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.ErgebnisKonsultation) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.ErgebnisKonsultation.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.CardExceptionContent) {
              throw (kse_service.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.OfflineRecord doKonsultationOffline(java.lang.String cin, java.lang.String dialogId, java.lang.String svNummer, java.lang.String svtCode, java.lang.String fachgebietsCode, java.lang.String behandlungsfallCode, java.lang.String nacherfassungsgrundCode, java.lang.String behandlungsdatum, java.lang.Boolean forceExecution, java.lang.String cardReaderId) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent, kse_service.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[6]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "doKonsultationOffline"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {cin, dialogId, svNummer, svtCode, fachgebietsCode, behandlungsfallCode, nacherfassungsgrundCode, behandlungsdatum, forceExecution, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.OfflineRecord) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.OfflineRecord) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.OfflineRecord.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.CardExceptionContent) {
              throw (kse_service.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.Konsultationsdaten[] downloadKonsultationsdaten(java.lang.String dialogId, java.lang.String anfrageId) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[7]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "downloadKonsultationsdaten"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, anfrageId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.Konsultationsdaten[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.Konsultationsdaten[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.Konsultationsdaten[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.BaseProperty[] getBehandlungsfaelle() throws java.rmi.RemoteException, kse_service.ServiceExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[8]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getBehandlungsfaelle"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.BaseProperty[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.BaseProperty[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.BaseProperty[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.BaseProperty[] getBehandlungsfaelleByFachgebiet(java.lang.String fachgebietCode) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.ServiceExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[9]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getBehandlungsfaelleByFachgebiet"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {fachgebietCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.BaseProperty[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.BaseProperty[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.BaseProperty[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.BehandlungsfallZusatzProperty[] getBehandlungsfaelleByFachgebietZusatz(java.lang.String fachgebietCode) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.ServiceExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[10]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getBehandlungsfaelleByFachgebietZusatz"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {fachgebietCode});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.BehandlungsfallZusatzProperty[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.BehandlungsfallZusatzProperty[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.BehandlungsfallZusatzProperty[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.Konsultationsdaten[] getKonsultationsdaten(java.lang.String dialogId, kse_service.SuchFilter selektionsKriterien, java.lang.String selektionsart, java.lang.String sortierung) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[11]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getKonsultationsdaten"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, selektionsKriterien, selektionsart, sortierung});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.Konsultationsdaten[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.Konsultationsdaten[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.Konsultationsdaten[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.KonsultationdatenAnfrage[] getKonsultationsdatenAnfragen(java.lang.String dialogId, java.lang.Boolean onlyReady) throws java.rmi.RemoteException, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[12]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getKonsultationsdatenAnfragen"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, onlyReady});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.KonsultationdatenAnfrage[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.KonsultationdatenAnfrage[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.KonsultationdatenAnfrage[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.Limit[] getLimit(java.lang.String dialogId, java.lang.Integer zeitraum) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[13]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getLimit"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, zeitraum});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.Limit[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.Limit[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.Limit[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public java.lang.Long getNumberOfflineKonsultationen(java.lang.String dialogId) throws java.rmi.RemoteException, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[14]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getNumberOfflineKonsultationen"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.Long) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.Long) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.Long.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public java.lang.Long getNumberOfflineNachsignaturen(java.lang.String dialogId) throws java.rmi.RemoteException, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[15]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getNumberOfflineNachsignaturen"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.Long) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.Long) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.Long.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.OfflineRecord[] getOfflineRecords(java.lang.String dialogId) throws java.rmi.RemoteException, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[16]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "getOfflineRecords"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.OfflineRecord[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.OfflineRecord[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.OfflineRecord[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.KonsultationsBeleg[] nachsignierenKonsultationen(java.lang.String dialogId, java.lang.String cin, java.lang.Boolean forceExecution, java.lang.String cardReaderId) throws java.rmi.RemoteException, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent, kse_service.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[17]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "nachsignierenKonsultationen"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, cin, forceExecution, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.KonsultationsBeleg[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.KonsultationsBeleg[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.KonsultationsBeleg[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.CardExceptionContent) {
              throw (kse_service.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.OfflineRecord nachsignierenKonsultationenOffline(java.lang.String dialogId, java.lang.String cin, java.lang.Boolean forceExecution, java.lang.String cardReaderId) throws java.rmi.RemoteException, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent, kse_service.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[18]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "nachsignierenKonsultationenOffline"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, cin, forceExecution, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.OfflineRecord) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.OfflineRecord) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.OfflineRecord.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.CardExceptionContent) {
              throw (kse_service.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public java.lang.String sendKonsultationsdatenAnfrage(java.lang.String dialogId, kse_service.SuchFilter selektionsKriterien) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[19]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "sendKonsultationsdatenAnfrage"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, selektionsKriterien});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.ErgebnisKonsultation sendOfflineKonsultation(java.lang.String dialogId, java.lang.String offlineId, java.lang.String fachgebietsCode, java.lang.String behandlungsfallCode, java.lang.String svtCode, java.lang.String anspruchId) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[20]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "sendOfflineKonsultation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, offlineId, fachgebietsCode, behandlungsfallCode, svtCode, anspruchId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.ErgebnisKonsultation) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.ErgebnisKonsultation) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.ErgebnisKonsultation.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.KonsultationsBeleg[] sendOfflineNachsignatur(java.lang.String dialogId, java.lang.String offlineId) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[21]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "sendOfflineNachsignatur"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, offlineId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.KonsultationsBeleg[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.KonsultationsBeleg[]) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.KonsultationsBeleg[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void sendZusatzinformationsAntwort(java.lang.String dialogId, java.lang.Long zusatzinformationsAntwortId, java.lang.Integer zusatzinformationsAntwort) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[22]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "sendZusatzinformationsAntwort"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, zusatzinformationsAntwortId, zusatzinformationsAntwort});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.KonsultationsBeleg stornierenKonsultation(java.lang.String dialogId, java.lang.Long konsId, java.lang.Integer konsVersion) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[23]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "stornierenKonsultation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, konsId, konsVersion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.KonsultationsBeleg) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.KonsultationsBeleg) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.KonsultationsBeleg.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public kse_service.ErgebnisKonsultation wiederinkraftsetzenKonsultation(java.lang.String dialogId, java.lang.Long konsId, java.lang.Integer konsVersion) throws java.rmi.RemoteException, kse_service.InvalidParameterExceptionContent, kse_service.KseExceptionContent, kse_service.AccessExceptionContent, kse_service.ServiceExceptionContent, kse_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[24]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "wiederinkraftsetzenKonsultation"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, konsId, konsVersion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (kse_service.ErgebnisKonsultation) _resp;
            } catch (java.lang.Exception _exception) {
                return (kse_service.ErgebnisKonsultation) org.apache.axis.utils.JavaUtils.convert(_resp, kse_service.ErgebnisKonsultation.class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.InvalidParameterExceptionContent) {
              throw (kse_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.KseExceptionContent) {
              throw (kse_service.KseExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.AccessExceptionContent) {
              throw (kse_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.ServiceExceptionContent) {
              throw (kse_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof kse_service.DialogExceptionContent) {
              throw (kse_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
