/**
 * KseExceptionContent.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package kse_service;

public class KseExceptionContent  extends kse_service.BaseExceptionContent  implements java.io.Serializable {
    private kse_service.Anspruch[] anspruch;

    public KseExceptionContent() {
    }

    public KseExceptionContent(
           java.lang.String code,
           java.lang.Integer errorCode,
           java.lang.String message1,
           kse_service.Anspruch[] anspruch) {
        super(
            code,
            errorCode,
            message1);
        this.anspruch = anspruch;
    }


    /**
     * Gets the anspruch value for this KseExceptionContent.
     * 
     * @return anspruch
     */
    public kse_service.Anspruch[] getAnspruch() {
        return anspruch;
    }


    /**
     * Sets the anspruch value for this KseExceptionContent.
     * 
     * @param anspruch
     */
    public void setAnspruch(kse_service.Anspruch[] anspruch) {
        this.anspruch = anspruch;
    }

    public kse_service.Anspruch getAnspruch(int i) {
        return this.anspruch[i];
    }

    public void setAnspruch(int i, kse_service.Anspruch _value) {
        this.anspruch[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof KseExceptionContent)) return false;
        KseExceptionContent other = (KseExceptionContent) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = super.equals(obj) && 
            ((this.anspruch==null && other.getAnspruch()==null) || 
             (this.anspruch!=null &&
              java.util.Arrays.equals(this.anspruch, other.getAnspruch())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = super.hashCode();
        if (getAnspruch() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAnspruch());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAnspruch(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(KseExceptionContent.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "kseExceptionContent"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anspruch");
        elemField.setXmlName(new javax.xml.namespace.QName("http://exceptions.soap.kse.client.chipkarte.at", "anspruch"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "anspruch"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }


    /**
     * Writes the exception data to the faultDetails
     */
    public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
        context.serialize(qname, null, this);
    }
}
