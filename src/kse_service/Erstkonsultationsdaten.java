/**
 * Erstkonsultationsdaten.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package kse_service;

public class Erstkonsultationsdaten  implements java.io.Serializable {
    private java.lang.String svNummer;

    private java.lang.String svNummerAbgeleitet;

    private java.lang.String anspruchId;

    private java.lang.String abgeleitetNachname;

    private java.lang.String abgeleitetVorname;

    private java.lang.String abgeleitetDruckNachname;

    private java.lang.String abgeleitetDruckVorname;

    private java.lang.String svtCode;

    private java.lang.String fachgebietsCode;

    private java.lang.String behandlungsfallCode;

    private java.lang.String behandlungsDatum;

    private java.lang.String konsultationsartCode;

    public Erstkonsultationsdaten() {
    }

    public Erstkonsultationsdaten(
           java.lang.String svNummer,
           java.lang.String svNummerAbgeleitet,
           java.lang.String anspruchId,
           java.lang.String abgeleitetNachname,
           java.lang.String abgeleitetVorname,
           java.lang.String abgeleitetDruckNachname,
           java.lang.String abgeleitetDruckVorname,
           java.lang.String svtCode,
           java.lang.String fachgebietsCode,
           java.lang.String behandlungsfallCode,
           java.lang.String behandlungsDatum,
           java.lang.String konsultationsartCode) {
           this.svNummer = svNummer;
           this.svNummerAbgeleitet = svNummerAbgeleitet;
           this.anspruchId = anspruchId;
           this.abgeleitetNachname = abgeleitetNachname;
           this.abgeleitetVorname = abgeleitetVorname;
           this.abgeleitetDruckNachname = abgeleitetDruckNachname;
           this.abgeleitetDruckVorname = abgeleitetDruckVorname;
           this.svtCode = svtCode;
           this.fachgebietsCode = fachgebietsCode;
           this.behandlungsfallCode = behandlungsfallCode;
           this.behandlungsDatum = behandlungsDatum;
           this.konsultationsartCode = konsultationsartCode;
    }


    /**
     * Gets the svNummer value for this Erstkonsultationsdaten.
     * 
     * @return svNummer
     */
    public java.lang.String getSvNummer() {
        return svNummer;
    }


    /**
     * Sets the svNummer value for this Erstkonsultationsdaten.
     * 
     * @param svNummer
     */
    public void setSvNummer(java.lang.String svNummer) {
        this.svNummer = svNummer;
    }


    /**
     * Gets the svNummerAbgeleitet value for this Erstkonsultationsdaten.
     * 
     * @return svNummerAbgeleitet
     */
    public java.lang.String getSvNummerAbgeleitet() {
        return svNummerAbgeleitet;
    }


    /**
     * Sets the svNummerAbgeleitet value for this Erstkonsultationsdaten.
     * 
     * @param svNummerAbgeleitet
     */
    public void setSvNummerAbgeleitet(java.lang.String svNummerAbgeleitet) {
        this.svNummerAbgeleitet = svNummerAbgeleitet;
    }


    /**
     * Gets the anspruchId value for this Erstkonsultationsdaten.
     * 
     * @return anspruchId
     */
    public java.lang.String getAnspruchId() {
        return anspruchId;
    }


    /**
     * Sets the anspruchId value for this Erstkonsultationsdaten.
     * 
     * @param anspruchId
     */
    public void setAnspruchId(java.lang.String anspruchId) {
        this.anspruchId = anspruchId;
    }


    /**
     * Gets the abgeleitetNachname value for this Erstkonsultationsdaten.
     * 
     * @return abgeleitetNachname
     */
    public java.lang.String getAbgeleitetNachname() {
        return abgeleitetNachname;
    }


    /**
     * Sets the abgeleitetNachname value for this Erstkonsultationsdaten.
     * 
     * @param abgeleitetNachname
     */
    public void setAbgeleitetNachname(java.lang.String abgeleitetNachname) {
        this.abgeleitetNachname = abgeleitetNachname;
    }


    /**
     * Gets the abgeleitetVorname value for this Erstkonsultationsdaten.
     * 
     * @return abgeleitetVorname
     */
    public java.lang.String getAbgeleitetVorname() {
        return abgeleitetVorname;
    }


    /**
     * Sets the abgeleitetVorname value for this Erstkonsultationsdaten.
     * 
     * @param abgeleitetVorname
     */
    public void setAbgeleitetVorname(java.lang.String abgeleitetVorname) {
        this.abgeleitetVorname = abgeleitetVorname;
    }


    /**
     * Gets the abgeleitetDruckNachname value for this Erstkonsultationsdaten.
     * 
     * @return abgeleitetDruckNachname
     */
    public java.lang.String getAbgeleitetDruckNachname() {
        return abgeleitetDruckNachname;
    }


    /**
     * Sets the abgeleitetDruckNachname value for this Erstkonsultationsdaten.
     * 
     * @param abgeleitetDruckNachname
     */
    public void setAbgeleitetDruckNachname(java.lang.String abgeleitetDruckNachname) {
        this.abgeleitetDruckNachname = abgeleitetDruckNachname;
    }


    /**
     * Gets the abgeleitetDruckVorname value for this Erstkonsultationsdaten.
     * 
     * @return abgeleitetDruckVorname
     */
    public java.lang.String getAbgeleitetDruckVorname() {
        return abgeleitetDruckVorname;
    }


    /**
     * Sets the abgeleitetDruckVorname value for this Erstkonsultationsdaten.
     * 
     * @param abgeleitetDruckVorname
     */
    public void setAbgeleitetDruckVorname(java.lang.String abgeleitetDruckVorname) {
        this.abgeleitetDruckVorname = abgeleitetDruckVorname;
    }


    /**
     * Gets the svtCode value for this Erstkonsultationsdaten.
     * 
     * @return svtCode
     */
    public java.lang.String getSvtCode() {
        return svtCode;
    }


    /**
     * Sets the svtCode value for this Erstkonsultationsdaten.
     * 
     * @param svtCode
     */
    public void setSvtCode(java.lang.String svtCode) {
        this.svtCode = svtCode;
    }


    /**
     * Gets the fachgebietsCode value for this Erstkonsultationsdaten.
     * 
     * @return fachgebietsCode
     */
    public java.lang.String getFachgebietsCode() {
        return fachgebietsCode;
    }


    /**
     * Sets the fachgebietsCode value for this Erstkonsultationsdaten.
     * 
     * @param fachgebietsCode
     */
    public void setFachgebietsCode(java.lang.String fachgebietsCode) {
        this.fachgebietsCode = fachgebietsCode;
    }


    /**
     * Gets the behandlungsfallCode value for this Erstkonsultationsdaten.
     * 
     * @return behandlungsfallCode
     */
    public java.lang.String getBehandlungsfallCode() {
        return behandlungsfallCode;
    }


    /**
     * Sets the behandlungsfallCode value for this Erstkonsultationsdaten.
     * 
     * @param behandlungsfallCode
     */
    public void setBehandlungsfallCode(java.lang.String behandlungsfallCode) {
        this.behandlungsfallCode = behandlungsfallCode;
    }


    /**
     * Gets the behandlungsDatum value for this Erstkonsultationsdaten.
     * 
     * @return behandlungsDatum
     */
    public java.lang.String getBehandlungsDatum() {
        return behandlungsDatum;
    }


    /**
     * Sets the behandlungsDatum value for this Erstkonsultationsdaten.
     * 
     * @param behandlungsDatum
     */
    public void setBehandlungsDatum(java.lang.String behandlungsDatum) {
        this.behandlungsDatum = behandlungsDatum;
    }


    /**
     * Gets the konsultationsartCode value for this Erstkonsultationsdaten.
     * 
     * @return konsultationsartCode
     */
    public java.lang.String getKonsultationsartCode() {
        return konsultationsartCode;
    }


    /**
     * Sets the konsultationsartCode value for this Erstkonsultationsdaten.
     * 
     * @param konsultationsartCode
     */
    public void setKonsultationsartCode(java.lang.String konsultationsartCode) {
        this.konsultationsartCode = konsultationsartCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Erstkonsultationsdaten)) return false;
        Erstkonsultationsdaten other = (Erstkonsultationsdaten) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.svNummer==null && other.getSvNummer()==null) || 
             (this.svNummer!=null &&
              this.svNummer.equals(other.getSvNummer()))) &&
            ((this.svNummerAbgeleitet==null && other.getSvNummerAbgeleitet()==null) || 
             (this.svNummerAbgeleitet!=null &&
              this.svNummerAbgeleitet.equals(other.getSvNummerAbgeleitet()))) &&
            ((this.anspruchId==null && other.getAnspruchId()==null) || 
             (this.anspruchId!=null &&
              this.anspruchId.equals(other.getAnspruchId()))) &&
            ((this.abgeleitetNachname==null && other.getAbgeleitetNachname()==null) || 
             (this.abgeleitetNachname!=null &&
              this.abgeleitetNachname.equals(other.getAbgeleitetNachname()))) &&
            ((this.abgeleitetVorname==null && other.getAbgeleitetVorname()==null) || 
             (this.abgeleitetVorname!=null &&
              this.abgeleitetVorname.equals(other.getAbgeleitetVorname()))) &&
            ((this.abgeleitetDruckNachname==null && other.getAbgeleitetDruckNachname()==null) || 
             (this.abgeleitetDruckNachname!=null &&
              this.abgeleitetDruckNachname.equals(other.getAbgeleitetDruckNachname()))) &&
            ((this.abgeleitetDruckVorname==null && other.getAbgeleitetDruckVorname()==null) || 
             (this.abgeleitetDruckVorname!=null &&
              this.abgeleitetDruckVorname.equals(other.getAbgeleitetDruckVorname()))) &&
            ((this.svtCode==null && other.getSvtCode()==null) || 
             (this.svtCode!=null &&
              this.svtCode.equals(other.getSvtCode()))) &&
            ((this.fachgebietsCode==null && other.getFachgebietsCode()==null) || 
             (this.fachgebietsCode!=null &&
              this.fachgebietsCode.equals(other.getFachgebietsCode()))) &&
            ((this.behandlungsfallCode==null && other.getBehandlungsfallCode()==null) || 
             (this.behandlungsfallCode!=null &&
              this.behandlungsfallCode.equals(other.getBehandlungsfallCode()))) &&
            ((this.behandlungsDatum==null && other.getBehandlungsDatum()==null) || 
             (this.behandlungsDatum!=null &&
              this.behandlungsDatum.equals(other.getBehandlungsDatum()))) &&
            ((this.konsultationsartCode==null && other.getKonsultationsartCode()==null) || 
             (this.konsultationsartCode!=null &&
              this.konsultationsartCode.equals(other.getKonsultationsartCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSvNummer() != null) {
            _hashCode += getSvNummer().hashCode();
        }
        if (getSvNummerAbgeleitet() != null) {
            _hashCode += getSvNummerAbgeleitet().hashCode();
        }
        if (getAnspruchId() != null) {
            _hashCode += getAnspruchId().hashCode();
        }
        if (getAbgeleitetNachname() != null) {
            _hashCode += getAbgeleitetNachname().hashCode();
        }
        if (getAbgeleitetVorname() != null) {
            _hashCode += getAbgeleitetVorname().hashCode();
        }
        if (getAbgeleitetDruckNachname() != null) {
            _hashCode += getAbgeleitetDruckNachname().hashCode();
        }
        if (getAbgeleitetDruckVorname() != null) {
            _hashCode += getAbgeleitetDruckVorname().hashCode();
        }
        if (getSvtCode() != null) {
            _hashCode += getSvtCode().hashCode();
        }
        if (getFachgebietsCode() != null) {
            _hashCode += getFachgebietsCode().hashCode();
        }
        if (getBehandlungsfallCode() != null) {
            _hashCode += getBehandlungsfallCode().hashCode();
        }
        if (getBehandlungsDatum() != null) {
            _hashCode += getBehandlungsDatum().hashCode();
        }
        if (getKonsultationsartCode() != null) {
            _hashCode += getKonsultationsartCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Erstkonsultationsdaten.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "erstkonsultationsdaten"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("svNummer");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "svNummer"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("svNummerAbgeleitet");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "svNummerAbgeleitet"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("anspruchId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "anspruchId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abgeleitetNachname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "abgeleitetNachname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abgeleitetVorname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "abgeleitetVorname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abgeleitetDruckNachname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "abgeleitetDruckNachname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("abgeleitetDruckVorname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "abgeleitetDruckVorname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("svtCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "svtCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fachgebietsCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "fachgebietsCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("behandlungsfallCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsfallCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("behandlungsDatum");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "behandlungsDatum"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("konsultationsartCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.kse.client.chipkarte.at", "konsultationsartCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
