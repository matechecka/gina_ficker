/**
 * IekoMedicament.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package das_service;

public class IekoMedicament  implements java.io.Serializable {
    private java.lang.String amount;

    private java.lang.String atcCode;

    private java.lang.String box;

    private java.lang.String dispensingCount;

    private java.lang.String divisibility;

    private java.lang.String dosageForm;

    private java.lang.String healthInsuranceSign;

    private das_service.IekoIngredient[] IEKOIngredient;

    private java.lang.String indicationText;

    private java.lang.String indicationTextHTMLFormatted;

    private java.lang.String informationText;

    private java.lang.String ingredientInformation;

    private java.lang.String ingredientName;

    private java.lang.String ingredientNarcoticSign;

    private java.lang.String longTermPermission;

    private java.lang.String medicamentName;

    private java.lang.String packageInformation;

    private java.lang.String packageNumber;

    private java.lang.String pharmaNumber;

    private java.lang.String price;

    private java.lang.String pricePerUnit;

    private java.lang.String preismodell;

    private java.lang.String registerNumber;

    private java.lang.String registerNumberPrefix;

    private java.lang.String ruleText;

    private java.lang.String ruleTextHTMLFormatted;

    public IekoMedicament() {
    }

    public IekoMedicament(
           java.lang.String amount,
           java.lang.String atcCode,
           java.lang.String box,
           java.lang.String dispensingCount,
           java.lang.String divisibility,
           java.lang.String dosageForm,
           java.lang.String healthInsuranceSign,
           das_service.IekoIngredient[] IEKOIngredient,
           java.lang.String indicationText,
           java.lang.String indicationTextHTMLFormatted,
           java.lang.String informationText,
           java.lang.String ingredientInformation,
           java.lang.String ingredientName,
           java.lang.String ingredientNarcoticSign,
           java.lang.String longTermPermission,
           java.lang.String medicamentName,
           java.lang.String packageInformation,
           java.lang.String packageNumber,
           java.lang.String pharmaNumber,
           java.lang.String price,
           java.lang.String pricePerUnit,
           java.lang.String preismodell,
           java.lang.String registerNumber,
           java.lang.String registerNumberPrefix,
           java.lang.String ruleText,
           java.lang.String ruleTextHTMLFormatted) {
           this.amount = amount;
           this.atcCode = atcCode;
           this.box = box;
           this.dispensingCount = dispensingCount;
           this.divisibility = divisibility;
           this.dosageForm = dosageForm;
           this.healthInsuranceSign = healthInsuranceSign;
           this.IEKOIngredient = IEKOIngredient;
           this.indicationText = indicationText;
           this.indicationTextHTMLFormatted = indicationTextHTMLFormatted;
           this.informationText = informationText;
           this.ingredientInformation = ingredientInformation;
           this.ingredientName = ingredientName;
           this.ingredientNarcoticSign = ingredientNarcoticSign;
           this.longTermPermission = longTermPermission;
           this.medicamentName = medicamentName;
           this.packageInformation = packageInformation;
           this.packageNumber = packageNumber;
           this.pharmaNumber = pharmaNumber;
           this.price = price;
           this.pricePerUnit = pricePerUnit;
           this.preismodell = preismodell;
           this.registerNumber = registerNumber;
           this.registerNumberPrefix = registerNumberPrefix;
           this.ruleText = ruleText;
           this.ruleTextHTMLFormatted = ruleTextHTMLFormatted;
    }


    /**
     * Gets the amount value for this IekoMedicament.
     * 
     * @return amount
     */
    public java.lang.String getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this IekoMedicament.
     * 
     * @param amount
     */
    public void setAmount(java.lang.String amount) {
        this.amount = amount;
    }


    /**
     * Gets the atcCode value for this IekoMedicament.
     * 
     * @return atcCode
     */
    public java.lang.String getAtcCode() {
        return atcCode;
    }


    /**
     * Sets the atcCode value for this IekoMedicament.
     * 
     * @param atcCode
     */
    public void setAtcCode(java.lang.String atcCode) {
        this.atcCode = atcCode;
    }


    /**
     * Gets the box value for this IekoMedicament.
     * 
     * @return box
     */
    public java.lang.String getBox() {
        return box;
    }


    /**
     * Sets the box value for this IekoMedicament.
     * 
     * @param box
     */
    public void setBox(java.lang.String box) {
        this.box = box;
    }


    /**
     * Gets the dispensingCount value for this IekoMedicament.
     * 
     * @return dispensingCount
     */
    public java.lang.String getDispensingCount() {
        return dispensingCount;
    }


    /**
     * Sets the dispensingCount value for this IekoMedicament.
     * 
     * @param dispensingCount
     */
    public void setDispensingCount(java.lang.String dispensingCount) {
        this.dispensingCount = dispensingCount;
    }


    /**
     * Gets the divisibility value for this IekoMedicament.
     * 
     * @return divisibility
     */
    public java.lang.String getDivisibility() {
        return divisibility;
    }


    /**
     * Sets the divisibility value for this IekoMedicament.
     * 
     * @param divisibility
     */
    public void setDivisibility(java.lang.String divisibility) {
        this.divisibility = divisibility;
    }


    /**
     * Gets the dosageForm value for this IekoMedicament.
     * 
     * @return dosageForm
     */
    public java.lang.String getDosageForm() {
        return dosageForm;
    }


    /**
     * Sets the dosageForm value for this IekoMedicament.
     * 
     * @param dosageForm
     */
    public void setDosageForm(java.lang.String dosageForm) {
        this.dosageForm = dosageForm;
    }


    /**
     * Gets the healthInsuranceSign value for this IekoMedicament.
     * 
     * @return healthInsuranceSign
     */
    public java.lang.String getHealthInsuranceSign() {
        return healthInsuranceSign;
    }


    /**
     * Sets the healthInsuranceSign value for this IekoMedicament.
     * 
     * @param healthInsuranceSign
     */
    public void setHealthInsuranceSign(java.lang.String healthInsuranceSign) {
        this.healthInsuranceSign = healthInsuranceSign;
    }


    /**
     * Gets the IEKOIngredient value for this IekoMedicament.
     * 
     * @return IEKOIngredient
     */
    public das_service.IekoIngredient[] getIEKOIngredient() {
        return IEKOIngredient;
    }


    /**
     * Sets the IEKOIngredient value for this IekoMedicament.
     * 
     * @param IEKOIngredient
     */
    public void setIEKOIngredient(das_service.IekoIngredient[] IEKOIngredient) {
        this.IEKOIngredient = IEKOIngredient;
    }

    public das_service.IekoIngredient getIEKOIngredient(int i) {
        return this.IEKOIngredient[i];
    }

    public void setIEKOIngredient(int i, das_service.IekoIngredient _value) {
        this.IEKOIngredient[i] = _value;
    }


    /**
     * Gets the indicationText value for this IekoMedicament.
     * 
     * @return indicationText
     */
    public java.lang.String getIndicationText() {
        return indicationText;
    }


    /**
     * Sets the indicationText value for this IekoMedicament.
     * 
     * @param indicationText
     */
    public void setIndicationText(java.lang.String indicationText) {
        this.indicationText = indicationText;
    }


    /**
     * Gets the indicationTextHTMLFormatted value for this IekoMedicament.
     * 
     * @return indicationTextHTMLFormatted
     */
    public java.lang.String getIndicationTextHTMLFormatted() {
        return indicationTextHTMLFormatted;
    }


    /**
     * Sets the indicationTextHTMLFormatted value for this IekoMedicament.
     * 
     * @param indicationTextHTMLFormatted
     */
    public void setIndicationTextHTMLFormatted(java.lang.String indicationTextHTMLFormatted) {
        this.indicationTextHTMLFormatted = indicationTextHTMLFormatted;
    }


    /**
     * Gets the informationText value for this IekoMedicament.
     * 
     * @return informationText
     */
    public java.lang.String getInformationText() {
        return informationText;
    }


    /**
     * Sets the informationText value for this IekoMedicament.
     * 
     * @param informationText
     */
    public void setInformationText(java.lang.String informationText) {
        this.informationText = informationText;
    }


    /**
     * Gets the ingredientInformation value for this IekoMedicament.
     * 
     * @return ingredientInformation
     */
    public java.lang.String getIngredientInformation() {
        return ingredientInformation;
    }


    /**
     * Sets the ingredientInformation value for this IekoMedicament.
     * 
     * @param ingredientInformation
     */
    public void setIngredientInformation(java.lang.String ingredientInformation) {
        this.ingredientInformation = ingredientInformation;
    }


    /**
     * Gets the ingredientName value for this IekoMedicament.
     * 
     * @return ingredientName
     */
    public java.lang.String getIngredientName() {
        return ingredientName;
    }


    /**
     * Sets the ingredientName value for this IekoMedicament.
     * 
     * @param ingredientName
     */
    public void setIngredientName(java.lang.String ingredientName) {
        this.ingredientName = ingredientName;
    }


    /**
     * Gets the ingredientNarcoticSign value for this IekoMedicament.
     * 
     * @return ingredientNarcoticSign
     */
    public java.lang.String getIngredientNarcoticSign() {
        return ingredientNarcoticSign;
    }


    /**
     * Sets the ingredientNarcoticSign value for this IekoMedicament.
     * 
     * @param ingredientNarcoticSign
     */
    public void setIngredientNarcoticSign(java.lang.String ingredientNarcoticSign) {
        this.ingredientNarcoticSign = ingredientNarcoticSign;
    }


    /**
     * Gets the longTermPermission value for this IekoMedicament.
     * 
     * @return longTermPermission
     */
    public java.lang.String getLongTermPermission() {
        return longTermPermission;
    }


    /**
     * Sets the longTermPermission value for this IekoMedicament.
     * 
     * @param longTermPermission
     */
    public void setLongTermPermission(java.lang.String longTermPermission) {
        this.longTermPermission = longTermPermission;
    }


    /**
     * Gets the medicamentName value for this IekoMedicament.
     * 
     * @return medicamentName
     */
    public java.lang.String getMedicamentName() {
        return medicamentName;
    }


    /**
     * Sets the medicamentName value for this IekoMedicament.
     * 
     * @param medicamentName
     */
    public void setMedicamentName(java.lang.String medicamentName) {
        this.medicamentName = medicamentName;
    }


    /**
     * Gets the packageInformation value for this IekoMedicament.
     * 
     * @return packageInformation
     */
    public java.lang.String getPackageInformation() {
        return packageInformation;
    }


    /**
     * Sets the packageInformation value for this IekoMedicament.
     * 
     * @param packageInformation
     */
    public void setPackageInformation(java.lang.String packageInformation) {
        this.packageInformation = packageInformation;
    }


    /**
     * Gets the packageNumber value for this IekoMedicament.
     * 
     * @return packageNumber
     */
    public java.lang.String getPackageNumber() {
        return packageNumber;
    }


    /**
     * Sets the packageNumber value for this IekoMedicament.
     * 
     * @param packageNumber
     */
    public void setPackageNumber(java.lang.String packageNumber) {
        this.packageNumber = packageNumber;
    }


    /**
     * Gets the pharmaNumber value for this IekoMedicament.
     * 
     * @return pharmaNumber
     */
    public java.lang.String getPharmaNumber() {
        return pharmaNumber;
    }


    /**
     * Sets the pharmaNumber value for this IekoMedicament.
     * 
     * @param pharmaNumber
     */
    public void setPharmaNumber(java.lang.String pharmaNumber) {
        this.pharmaNumber = pharmaNumber;
    }


    /**
     * Gets the price value for this IekoMedicament.
     * 
     * @return price
     */
    public java.lang.String getPrice() {
        return price;
    }


    /**
     * Sets the price value for this IekoMedicament.
     * 
     * @param price
     */
    public void setPrice(java.lang.String price) {
        this.price = price;
    }


    /**
     * Gets the pricePerUnit value for this IekoMedicament.
     * 
     * @return pricePerUnit
     */
    public java.lang.String getPricePerUnit() {
        return pricePerUnit;
    }


    /**
     * Sets the pricePerUnit value for this IekoMedicament.
     * 
     * @param pricePerUnit
     */
    public void setPricePerUnit(java.lang.String pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }


    /**
     * Gets the preismodell value for this IekoMedicament.
     * 
     * @return preismodell
     */
    public java.lang.String getPreismodell() {
        return preismodell;
    }


    /**
     * Sets the preismodell value for this IekoMedicament.
     * 
     * @param preismodell
     */
    public void setPreismodell(java.lang.String preismodell) {
        this.preismodell = preismodell;
    }


    /**
     * Gets the registerNumber value for this IekoMedicament.
     * 
     * @return registerNumber
     */
    public java.lang.String getRegisterNumber() {
        return registerNumber;
    }


    /**
     * Sets the registerNumber value for this IekoMedicament.
     * 
     * @param registerNumber
     */
    public void setRegisterNumber(java.lang.String registerNumber) {
        this.registerNumber = registerNumber;
    }


    /**
     * Gets the registerNumberPrefix value for this IekoMedicament.
     * 
     * @return registerNumberPrefix
     */
    public java.lang.String getRegisterNumberPrefix() {
        return registerNumberPrefix;
    }


    /**
     * Sets the registerNumberPrefix value for this IekoMedicament.
     * 
     * @param registerNumberPrefix
     */
    public void setRegisterNumberPrefix(java.lang.String registerNumberPrefix) {
        this.registerNumberPrefix = registerNumberPrefix;
    }


    /**
     * Gets the ruleText value for this IekoMedicament.
     * 
     * @return ruleText
     */
    public java.lang.String getRuleText() {
        return ruleText;
    }


    /**
     * Sets the ruleText value for this IekoMedicament.
     * 
     * @param ruleText
     */
    public void setRuleText(java.lang.String ruleText) {
        this.ruleText = ruleText;
    }


    /**
     * Gets the ruleTextHTMLFormatted value for this IekoMedicament.
     * 
     * @return ruleTextHTMLFormatted
     */
    public java.lang.String getRuleTextHTMLFormatted() {
        return ruleTextHTMLFormatted;
    }


    /**
     * Sets the ruleTextHTMLFormatted value for this IekoMedicament.
     * 
     * @param ruleTextHTMLFormatted
     */
    public void setRuleTextHTMLFormatted(java.lang.String ruleTextHTMLFormatted) {
        this.ruleTextHTMLFormatted = ruleTextHTMLFormatted;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IekoMedicament)) return false;
        IekoMedicament other = (IekoMedicament) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.amount==null && other.getAmount()==null) || 
             (this.amount!=null &&
              this.amount.equals(other.getAmount()))) &&
            ((this.atcCode==null && other.getAtcCode()==null) || 
             (this.atcCode!=null &&
              this.atcCode.equals(other.getAtcCode()))) &&
            ((this.box==null && other.getBox()==null) || 
             (this.box!=null &&
              this.box.equals(other.getBox()))) &&
            ((this.dispensingCount==null && other.getDispensingCount()==null) || 
             (this.dispensingCount!=null &&
              this.dispensingCount.equals(other.getDispensingCount()))) &&
            ((this.divisibility==null && other.getDivisibility()==null) || 
             (this.divisibility!=null &&
              this.divisibility.equals(other.getDivisibility()))) &&
            ((this.dosageForm==null && other.getDosageForm()==null) || 
             (this.dosageForm!=null &&
              this.dosageForm.equals(other.getDosageForm()))) &&
            ((this.healthInsuranceSign==null && other.getHealthInsuranceSign()==null) || 
             (this.healthInsuranceSign!=null &&
              this.healthInsuranceSign.equals(other.getHealthInsuranceSign()))) &&
            ((this.IEKOIngredient==null && other.getIEKOIngredient()==null) || 
             (this.IEKOIngredient!=null &&
              java.util.Arrays.equals(this.IEKOIngredient, other.getIEKOIngredient()))) &&
            ((this.indicationText==null && other.getIndicationText()==null) || 
             (this.indicationText!=null &&
              this.indicationText.equals(other.getIndicationText()))) &&
            ((this.indicationTextHTMLFormatted==null && other.getIndicationTextHTMLFormatted()==null) || 
             (this.indicationTextHTMLFormatted!=null &&
              this.indicationTextHTMLFormatted.equals(other.getIndicationTextHTMLFormatted()))) &&
            ((this.informationText==null && other.getInformationText()==null) || 
             (this.informationText!=null &&
              this.informationText.equals(other.getInformationText()))) &&
            ((this.ingredientInformation==null && other.getIngredientInformation()==null) || 
             (this.ingredientInformation!=null &&
              this.ingredientInformation.equals(other.getIngredientInformation()))) &&
            ((this.ingredientName==null && other.getIngredientName()==null) || 
             (this.ingredientName!=null &&
              this.ingredientName.equals(other.getIngredientName()))) &&
            ((this.ingredientNarcoticSign==null && other.getIngredientNarcoticSign()==null) || 
             (this.ingredientNarcoticSign!=null &&
              this.ingredientNarcoticSign.equals(other.getIngredientNarcoticSign()))) &&
            ((this.longTermPermission==null && other.getLongTermPermission()==null) || 
             (this.longTermPermission!=null &&
              this.longTermPermission.equals(other.getLongTermPermission()))) &&
            ((this.medicamentName==null && other.getMedicamentName()==null) || 
             (this.medicamentName!=null &&
              this.medicamentName.equals(other.getMedicamentName()))) &&
            ((this.packageInformation==null && other.getPackageInformation()==null) || 
             (this.packageInformation!=null &&
              this.packageInformation.equals(other.getPackageInformation()))) &&
            ((this.packageNumber==null && other.getPackageNumber()==null) || 
             (this.packageNumber!=null &&
              this.packageNumber.equals(other.getPackageNumber()))) &&
            ((this.pharmaNumber==null && other.getPharmaNumber()==null) || 
             (this.pharmaNumber!=null &&
              this.pharmaNumber.equals(other.getPharmaNumber()))) &&
            ((this.price==null && other.getPrice()==null) || 
             (this.price!=null &&
              this.price.equals(other.getPrice()))) &&
            ((this.pricePerUnit==null && other.getPricePerUnit()==null) || 
             (this.pricePerUnit!=null &&
              this.pricePerUnit.equals(other.getPricePerUnit()))) &&
            ((this.preismodell==null && other.getPreismodell()==null) || 
             (this.preismodell!=null &&
              this.preismodell.equals(other.getPreismodell()))) &&
            ((this.registerNumber==null && other.getRegisterNumber()==null) || 
             (this.registerNumber!=null &&
              this.registerNumber.equals(other.getRegisterNumber()))) &&
            ((this.registerNumberPrefix==null && other.getRegisterNumberPrefix()==null) || 
             (this.registerNumberPrefix!=null &&
              this.registerNumberPrefix.equals(other.getRegisterNumberPrefix()))) &&
            ((this.ruleText==null && other.getRuleText()==null) || 
             (this.ruleText!=null &&
              this.ruleText.equals(other.getRuleText()))) &&
            ((this.ruleTextHTMLFormatted==null && other.getRuleTextHTMLFormatted()==null) || 
             (this.ruleTextHTMLFormatted!=null &&
              this.ruleTextHTMLFormatted.equals(other.getRuleTextHTMLFormatted())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAmount() != null) {
            _hashCode += getAmount().hashCode();
        }
        if (getAtcCode() != null) {
            _hashCode += getAtcCode().hashCode();
        }
        if (getBox() != null) {
            _hashCode += getBox().hashCode();
        }
        if (getDispensingCount() != null) {
            _hashCode += getDispensingCount().hashCode();
        }
        if (getDivisibility() != null) {
            _hashCode += getDivisibility().hashCode();
        }
        if (getDosageForm() != null) {
            _hashCode += getDosageForm().hashCode();
        }
        if (getHealthInsuranceSign() != null) {
            _hashCode += getHealthInsuranceSign().hashCode();
        }
        if (getIEKOIngredient() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIEKOIngredient());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIEKOIngredient(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIndicationText() != null) {
            _hashCode += getIndicationText().hashCode();
        }
        if (getIndicationTextHTMLFormatted() != null) {
            _hashCode += getIndicationTextHTMLFormatted().hashCode();
        }
        if (getInformationText() != null) {
            _hashCode += getInformationText().hashCode();
        }
        if (getIngredientInformation() != null) {
            _hashCode += getIngredientInformation().hashCode();
        }
        if (getIngredientName() != null) {
            _hashCode += getIngredientName().hashCode();
        }
        if (getIngredientNarcoticSign() != null) {
            _hashCode += getIngredientNarcoticSign().hashCode();
        }
        if (getLongTermPermission() != null) {
            _hashCode += getLongTermPermission().hashCode();
        }
        if (getMedicamentName() != null) {
            _hashCode += getMedicamentName().hashCode();
        }
        if (getPackageInformation() != null) {
            _hashCode += getPackageInformation().hashCode();
        }
        if (getPackageNumber() != null) {
            _hashCode += getPackageNumber().hashCode();
        }
        if (getPharmaNumber() != null) {
            _hashCode += getPharmaNumber().hashCode();
        }
        if (getPrice() != null) {
            _hashCode += getPrice().hashCode();
        }
        if (getPricePerUnit() != null) {
            _hashCode += getPricePerUnit().hashCode();
        }
        if (getPreismodell() != null) {
            _hashCode += getPreismodell().hashCode();
        }
        if (getRegisterNumber() != null) {
            _hashCode += getRegisterNumber().hashCode();
        }
        if (getRegisterNumberPrefix() != null) {
            _hashCode += getRegisterNumberPrefix().hashCode();
        }
        if (getRuleText() != null) {
            _hashCode += getRuleText().hashCode();
        }
        if (getRuleTextHTMLFormatted() != null) {
            _hashCode += getRuleTextHTMLFormatted().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IekoMedicament.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoMedicament"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("atcCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "atcCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("box");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "box"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dispensingCount");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "dispensingCount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("divisibility");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "divisibility"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dosageForm");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "dosageForm"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("healthInsuranceSign");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "healthInsuranceSign"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IEKOIngredient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "IEKOIngredient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoIngredient"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indicationText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "indicationText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("indicationTextHTMLFormatted");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "indicationTextHTMLFormatted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("informationText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "informationText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ingredientInformation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "ingredientInformation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ingredientName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "ingredientName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ingredientNarcoticSign");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "ingredientNarcoticSign"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("longTermPermission");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "longTermPermission"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medicamentName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "medicamentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packageInformation");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "packageInformation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("packageNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "packageNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pharmaNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "pharmaNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("price");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "price"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pricePerUnit");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "pricePerUnit"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("preismodell");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "preismodell"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registerNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "registerNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("registerNumberPrefix");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "registerNumberPrefix"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleText");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "ruleText"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ruleTextHTMLFormatted");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "ruleTextHTMLFormatted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
