/**
 * IekoIngredientReference.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package das_service;

public class IekoIngredientReference  implements java.io.Serializable {
    private java.lang.String atcCode;

    private java.lang.String ingredientName;

    public IekoIngredientReference() {
    }

    public IekoIngredientReference(
           java.lang.String atcCode,
           java.lang.String ingredientName) {
           this.atcCode = atcCode;
           this.ingredientName = ingredientName;
    }


    /**
     * Gets the atcCode value for this IekoIngredientReference.
     * 
     * @return atcCode
     */
    public java.lang.String getAtcCode() {
        return atcCode;
    }


    /**
     * Sets the atcCode value for this IekoIngredientReference.
     * 
     * @param atcCode
     */
    public void setAtcCode(java.lang.String atcCode) {
        this.atcCode = atcCode;
    }


    /**
     * Gets the ingredientName value for this IekoIngredientReference.
     * 
     * @return ingredientName
     */
    public java.lang.String getIngredientName() {
        return ingredientName;
    }


    /**
     * Sets the ingredientName value for this IekoIngredientReference.
     * 
     * @param ingredientName
     */
    public void setIngredientName(java.lang.String ingredientName) {
        this.ingredientName = ingredientName;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IekoIngredientReference)) return false;
        IekoIngredientReference other = (IekoIngredientReference) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.atcCode==null && other.getAtcCode()==null) || 
             (this.atcCode!=null &&
              this.atcCode.equals(other.getAtcCode()))) &&
            ((this.ingredientName==null && other.getIngredientName()==null) || 
             (this.ingredientName!=null &&
              this.ingredientName.equals(other.getIngredientName())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAtcCode() != null) {
            _hashCode += getAtcCode().hashCode();
        }
        if (getIngredientName() != null) {
            _hashCode += getIngredientName().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IekoIngredientReference.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoIngredientReference"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("atcCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "atcCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ingredientName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "ingredientName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
