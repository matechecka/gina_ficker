/**
 * IDasService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package das_service;

public interface IDasService extends java.rmi.Remote {
    public das_service.Property[] checkStatus(java.lang.String dialogId) throws java.rmi.RemoteException, das_service.AccessExceptionContent, das_service.ServiceExceptionContent, das_service.DialogExceptionContent;
    public das_service.IekoEconomicGrouping retrieveIEKOEconomicGrouping(java.lang.String dialogId, java.lang.String pharmaNumber) throws java.rmi.RemoteException, das_service.AccessExceptionContent, das_service.ServiceExceptionContent, das_service.DialogExceptionContent, das_service.DasInvalidParameterExceptionContent, das_service.DasExceptionContent;
}
