/**
 * IekoComparison.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package das_service;

public class IekoComparison  implements java.io.Serializable {
    private java.lang.String comparisonCode;

    private java.lang.String comparisonPosition;

    private das_service.IekoMedicament IEKOMedicament;

    public IekoComparison() {
    }

    public IekoComparison(
           java.lang.String comparisonCode,
           java.lang.String comparisonPosition,
           das_service.IekoMedicament IEKOMedicament) {
           this.comparisonCode = comparisonCode;
           this.comparisonPosition = comparisonPosition;
           this.IEKOMedicament = IEKOMedicament;
    }


    /**
     * Gets the comparisonCode value for this IekoComparison.
     * 
     * @return comparisonCode
     */
    public java.lang.String getComparisonCode() {
        return comparisonCode;
    }


    /**
     * Sets the comparisonCode value for this IekoComparison.
     * 
     * @param comparisonCode
     */
    public void setComparisonCode(java.lang.String comparisonCode) {
        this.comparisonCode = comparisonCode;
    }


    /**
     * Gets the comparisonPosition value for this IekoComparison.
     * 
     * @return comparisonPosition
     */
    public java.lang.String getComparisonPosition() {
        return comparisonPosition;
    }


    /**
     * Sets the comparisonPosition value for this IekoComparison.
     * 
     * @param comparisonPosition
     */
    public void setComparisonPosition(java.lang.String comparisonPosition) {
        this.comparisonPosition = comparisonPosition;
    }


    /**
     * Gets the IEKOMedicament value for this IekoComparison.
     * 
     * @return IEKOMedicament
     */
    public das_service.IekoMedicament getIEKOMedicament() {
        return IEKOMedicament;
    }


    /**
     * Sets the IEKOMedicament value for this IekoComparison.
     * 
     * @param IEKOMedicament
     */
    public void setIEKOMedicament(das_service.IekoMedicament IEKOMedicament) {
        this.IEKOMedicament = IEKOMedicament;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IekoComparison)) return false;
        IekoComparison other = (IekoComparison) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.comparisonCode==null && other.getComparisonCode()==null) || 
             (this.comparisonCode!=null &&
              this.comparisonCode.equals(other.getComparisonCode()))) &&
            ((this.comparisonPosition==null && other.getComparisonPosition()==null) || 
             (this.comparisonPosition!=null &&
              this.comparisonPosition.equals(other.getComparisonPosition()))) &&
            ((this.IEKOMedicament==null && other.getIEKOMedicament()==null) || 
             (this.IEKOMedicament!=null &&
              this.IEKOMedicament.equals(other.getIEKOMedicament())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getComparisonCode() != null) {
            _hashCode += getComparisonCode().hashCode();
        }
        if (getComparisonPosition() != null) {
            _hashCode += getComparisonPosition().hashCode();
        }
        if (getIEKOMedicament() != null) {
            _hashCode += getIEKOMedicament().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IekoComparison.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoComparison"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comparisonCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "comparisonCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comparisonPosition");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "comparisonPosition"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IEKOMedicament");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "IEKOMedicament"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoMedicament"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
