/**
 * DasService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package das_service;

public interface DasService extends javax.xml.rpc.Service {
    public java.lang.String getdas_5Address();

    public das_service.IDasService getdas_5() throws javax.xml.rpc.ServiceException;

    public das_service.IDasService getdas_5(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
