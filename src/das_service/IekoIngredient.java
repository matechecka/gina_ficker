/**
 * IekoIngredient.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package das_service;

public class IekoIngredient  implements java.io.Serializable {
    private java.lang.String atcCode;

    private java.lang.String ingredientDimension;

    private java.lang.String ingredientName;

    private java.lang.String ingredientStrength;

    public IekoIngredient() {
    }

    public IekoIngredient(
           java.lang.String atcCode,
           java.lang.String ingredientDimension,
           java.lang.String ingredientName,
           java.lang.String ingredientStrength) {
           this.atcCode = atcCode;
           this.ingredientDimension = ingredientDimension;
           this.ingredientName = ingredientName;
           this.ingredientStrength = ingredientStrength;
    }


    /**
     * Gets the atcCode value for this IekoIngredient.
     * 
     * @return atcCode
     */
    public java.lang.String getAtcCode() {
        return atcCode;
    }


    /**
     * Sets the atcCode value for this IekoIngredient.
     * 
     * @param atcCode
     */
    public void setAtcCode(java.lang.String atcCode) {
        this.atcCode = atcCode;
    }


    /**
     * Gets the ingredientDimension value for this IekoIngredient.
     * 
     * @return ingredientDimension
     */
    public java.lang.String getIngredientDimension() {
        return ingredientDimension;
    }


    /**
     * Sets the ingredientDimension value for this IekoIngredient.
     * 
     * @param ingredientDimension
     */
    public void setIngredientDimension(java.lang.String ingredientDimension) {
        this.ingredientDimension = ingredientDimension;
    }


    /**
     * Gets the ingredientName value for this IekoIngredient.
     * 
     * @return ingredientName
     */
    public java.lang.String getIngredientName() {
        return ingredientName;
    }


    /**
     * Sets the ingredientName value for this IekoIngredient.
     * 
     * @param ingredientName
     */
    public void setIngredientName(java.lang.String ingredientName) {
        this.ingredientName = ingredientName;
    }


    /**
     * Gets the ingredientStrength value for this IekoIngredient.
     * 
     * @return ingredientStrength
     */
    public java.lang.String getIngredientStrength() {
        return ingredientStrength;
    }


    /**
     * Sets the ingredientStrength value for this IekoIngredient.
     * 
     * @param ingredientStrength
     */
    public void setIngredientStrength(java.lang.String ingredientStrength) {
        this.ingredientStrength = ingredientStrength;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IekoIngredient)) return false;
        IekoIngredient other = (IekoIngredient) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.atcCode==null && other.getAtcCode()==null) || 
             (this.atcCode!=null &&
              this.atcCode.equals(other.getAtcCode()))) &&
            ((this.ingredientDimension==null && other.getIngredientDimension()==null) || 
             (this.ingredientDimension!=null &&
              this.ingredientDimension.equals(other.getIngredientDimension()))) &&
            ((this.ingredientName==null && other.getIngredientName()==null) || 
             (this.ingredientName!=null &&
              this.ingredientName.equals(other.getIngredientName()))) &&
            ((this.ingredientStrength==null && other.getIngredientStrength()==null) || 
             (this.ingredientStrength!=null &&
              this.ingredientStrength.equals(other.getIngredientStrength())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAtcCode() != null) {
            _hashCode += getAtcCode().hashCode();
        }
        if (getIngredientDimension() != null) {
            _hashCode += getIngredientDimension().hashCode();
        }
        if (getIngredientName() != null) {
            _hashCode += getIngredientName().hashCode();
        }
        if (getIngredientStrength() != null) {
            _hashCode += getIngredientStrength().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IekoIngredient.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoIngredient"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("atcCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "atcCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ingredientDimension");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "ingredientDimension"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ingredientName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "ingredientName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ingredientStrength");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "ingredientStrength"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
