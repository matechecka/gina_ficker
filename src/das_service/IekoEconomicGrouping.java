/**
 * IekoEconomicGrouping.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package das_service;

public class IekoEconomicGrouping  implements java.io.Serializable {
    private das_service.IekoComparison[] IEKOComparison;

    private das_service.IekoIngredientReference identIngredient;

    private java.lang.String medicamentName;

    private java.lang.String pharmaNumber;

    private das_service.IekoIngredientReference similarIngredient;

    public IekoEconomicGrouping() {
    }

    public IekoEconomicGrouping(
           das_service.IekoComparison[] IEKOComparison,
           das_service.IekoIngredientReference identIngredient,
           java.lang.String medicamentName,
           java.lang.String pharmaNumber,
           das_service.IekoIngredientReference similarIngredient) {
           this.IEKOComparison = IEKOComparison;
           this.identIngredient = identIngredient;
           this.medicamentName = medicamentName;
           this.pharmaNumber = pharmaNumber;
           this.similarIngredient = similarIngredient;
    }


    /**
     * Gets the IEKOComparison value for this IekoEconomicGrouping.
     * 
     * @return IEKOComparison
     */
    public das_service.IekoComparison[] getIEKOComparison() {
        return IEKOComparison;
    }


    /**
     * Sets the IEKOComparison value for this IekoEconomicGrouping.
     * 
     * @param IEKOComparison
     */
    public void setIEKOComparison(das_service.IekoComparison[] IEKOComparison) {
        this.IEKOComparison = IEKOComparison;
    }

    public das_service.IekoComparison getIEKOComparison(int i) {
        return this.IEKOComparison[i];
    }

    public void setIEKOComparison(int i, das_service.IekoComparison _value) {
        this.IEKOComparison[i] = _value;
    }


    /**
     * Gets the identIngredient value for this IekoEconomicGrouping.
     * 
     * @return identIngredient
     */
    public das_service.IekoIngredientReference getIdentIngredient() {
        return identIngredient;
    }


    /**
     * Sets the identIngredient value for this IekoEconomicGrouping.
     * 
     * @param identIngredient
     */
    public void setIdentIngredient(das_service.IekoIngredientReference identIngredient) {
        this.identIngredient = identIngredient;
    }


    /**
     * Gets the medicamentName value for this IekoEconomicGrouping.
     * 
     * @return medicamentName
     */
    public java.lang.String getMedicamentName() {
        return medicamentName;
    }


    /**
     * Sets the medicamentName value for this IekoEconomicGrouping.
     * 
     * @param medicamentName
     */
    public void setMedicamentName(java.lang.String medicamentName) {
        this.medicamentName = medicamentName;
    }


    /**
     * Gets the pharmaNumber value for this IekoEconomicGrouping.
     * 
     * @return pharmaNumber
     */
    public java.lang.String getPharmaNumber() {
        return pharmaNumber;
    }


    /**
     * Sets the pharmaNumber value for this IekoEconomicGrouping.
     * 
     * @param pharmaNumber
     */
    public void setPharmaNumber(java.lang.String pharmaNumber) {
        this.pharmaNumber = pharmaNumber;
    }


    /**
     * Gets the similarIngredient value for this IekoEconomicGrouping.
     * 
     * @return similarIngredient
     */
    public das_service.IekoIngredientReference getSimilarIngredient() {
        return similarIngredient;
    }


    /**
     * Sets the similarIngredient value for this IekoEconomicGrouping.
     * 
     * @param similarIngredient
     */
    public void setSimilarIngredient(das_service.IekoIngredientReference similarIngredient) {
        this.similarIngredient = similarIngredient;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof IekoEconomicGrouping)) return false;
        IekoEconomicGrouping other = (IekoEconomicGrouping) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.IEKOComparison==null && other.getIEKOComparison()==null) || 
             (this.IEKOComparison!=null &&
              java.util.Arrays.equals(this.IEKOComparison, other.getIEKOComparison()))) &&
            ((this.identIngredient==null && other.getIdentIngredient()==null) || 
             (this.identIngredient!=null &&
              this.identIngredient.equals(other.getIdentIngredient()))) &&
            ((this.medicamentName==null && other.getMedicamentName()==null) || 
             (this.medicamentName!=null &&
              this.medicamentName.equals(other.getMedicamentName()))) &&
            ((this.pharmaNumber==null && other.getPharmaNumber()==null) || 
             (this.pharmaNumber!=null &&
              this.pharmaNumber.equals(other.getPharmaNumber()))) &&
            ((this.similarIngredient==null && other.getSimilarIngredient()==null) || 
             (this.similarIngredient!=null &&
              this.similarIngredient.equals(other.getSimilarIngredient())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIEKOComparison() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getIEKOComparison());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getIEKOComparison(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getIdentIngredient() != null) {
            _hashCode += getIdentIngredient().hashCode();
        }
        if (getMedicamentName() != null) {
            _hashCode += getMedicamentName().hashCode();
        }
        if (getPharmaNumber() != null) {
            _hashCode += getPharmaNumber().hashCode();
        }
        if (getSimilarIngredient() != null) {
            _hashCode += getSimilarIngredient().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(IekoEconomicGrouping.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoEconomicGrouping"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("IEKOComparison");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "IEKOComparison"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoComparison"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("identIngredient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "identIngredient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoIngredientReference"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("medicamentName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "medicamentName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pharmaNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "pharmaNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("similarIngredient");
        elemField.setXmlName(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "similarIngredient"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "iekoIngredientReference"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
