/**
 * DasServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package das_service;

public class DasServiceLocator extends org.apache.axis.client.Service implements das_service.DasService {

    public DasServiceLocator() {
    }


    public DasServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public DasServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for das_5
    private java.lang.String das_5_address = "https://10.196.2.18/das/5";

    public java.lang.String getdas_5Address() {
        return das_5_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String das_5WSDDServiceName = "das_5";

    public java.lang.String getdas_5WSDDServiceName() {
        return das_5WSDDServiceName;
    }

    public void setdas_5WSDDServiceName(java.lang.String name) {
        das_5WSDDServiceName = name;
    }

    public das_service.IDasService getdas_5() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(das_5_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getdas_5(endpoint);
    }

    public das_service.IDasService getdas_5(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            das_service.Das_5BindingStub _stub = new das_service.Das_5BindingStub(portAddress, this);
            _stub.setPortName(getdas_5WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setdas_5EndpointAddress(java.lang.String address) {
        das_5_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (das_service.IDasService.class.isAssignableFrom(serviceEndpointInterface)) {
                das_service.Das_5BindingStub _stub = new das_service.Das_5BindingStub(new java.net.URL(das_5_address), this);
                _stub.setPortName(getdas_5WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("das_5".equals(inputPortName)) {
            return getdas_5();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "DasService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://soap.das.client.chipkarte.at", "das_5"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("das_5".equals(portName)) {
            setdas_5EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
