import UI.MyContainer;
import base_service.*;

import javax.xml.rpc.ServiceException;
import java.lang.reflect.Array;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

public class Brot_O_Typ {
    public static void main(String[] args) {
        String dialogid = null;
        IBaseService client = null;
        Map<String, String> map = new HashMap<>();
        map.put("KU", "Kurativ");
        map.put("EC", "E-Card Partner");
        map.put("ELGA", "ELGA-Partner");
        map.put("GU", "Vorsorgeuntersuchung");
        map.put("HAPO", "Hausapothekenvertrag");
        map.put("REZ", "Rezeptur");

        try {

            BaseServiceLocator locatorm = new BaseServiceLocator();
            client = locatorm.getbase_15();
            CardReader[] readers = client.getCardReaders();

            ProduktInfo pi = new ProduktInfo();
            pi.setProduktId(1);
            pi.setProduktVersion("1");

            dialogid = client.createDialog(readers[0].getId(), pi, null, false );

            VertragspartnerV2 auth = client.authenticateDialog(dialogid,  null, "0000", readers[0].getId());
            Ordination[] ordination = auth.getOrdination();
            auth.getOrdination()[0].getTaetigkeitsBereich()[0].getId();
            TaetigkeitsBereich[] t1 = ordination[0].getTaetigkeitsBereich();


            client.setDialogAddress(dialogid, ordination[0].getOrdinationId(), t1[0].getId(), null, null, null );

            VertragsDaten[] v1 = client.getVertraege(dialogid);
            BaseProperty[] bp = client.getFachgebiete();
            SvtProperty[] svt = client.getSVTs();

            String[] berechtigungen = client.getBerechtigungen(dialogid);
            System.out.println("++++Vertragsdaten++++");

            for (VertragsDaten aV1 : v1) {
                String out = "";
                out += map.get(aV1.getVertragsTyp()) + "\t| ";

                for (SvtProperty aSvt : svt) {
                    if (aV1.getLeistungsSVT().equals(aSvt.getCode())) {

                        out += "LeistungsSVT: " + aSvt.getKurzbezeichnung() + "\t| ";
                    }
                    else{

                    }

                    if (aV1.getVerrechnungsSVT().equals(aSvt.getCode())) {

                        out += "VerrechnungsSVT: " + aSvt.getKurzbezeichnung() + "\t| ";
                    }
                    else{

                    }
                }

                for (BaseProperty b : bp) {

                    if (aV1.getFachGebietsCode().equals(b.getCode())) {
                        out += "Fachgebiet: " + b.getText() + "\t| ";

                    } else {
                        //System.out.println("unequal");
                    }

                }


                for (TaetigkeitsBereich aT1 : t1) {
                    if (aV1.getTaetigkeitsBereichId().equals(aT1.getId())) {
                        out += "Taetigkeitsbereich: " + aT1.getCode();

                    } else {
                        //System.out.println("unequal: " + aV1.getTaetigkeitsBereichId() + "," + (aT1.getId()));
                    }
                }

                System.out.println(out);
            }

            System.out.println("Berechtigungen: ");

            for (String b : berechtigungen) {
                System.out.println(b);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            if (dialogid != null){
                try {
                    client.closeDialog(dialogid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
