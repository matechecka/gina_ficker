/**
 * BaseService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package base_service;

public interface BaseService extends javax.xml.rpc.Service {
    public java.lang.String getbase_15Address();

    public base_service.IBaseService getbase_15() throws javax.xml.rpc.ServiceException;

    public base_service.IBaseService getbase_15(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
