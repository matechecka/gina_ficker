/**
 * TsvService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tsv_service;

public interface TsvService extends javax.xml.rpc.Service {
    public java.lang.String gettsv_3Address();

    public tsv_service.ITsvService gettsv_3() throws javax.xml.rpc.ServiceException;

    public tsv_service.ITsvService gettsv_3(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
