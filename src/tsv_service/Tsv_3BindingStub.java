/**
 * Tsv_3BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tsv_service;

public class Tsv_3BindingStub extends org.apache.axis.client.Stub implements tsv_service.ITsvService {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[4];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("checkStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "property"));
        oper.setReturnClass(tsv_service.Property[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "tsv_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "tsv_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "SchulungException"),
                      "tsv_service.SchulungExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "schulungExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "tsv_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("deleteSchulungsdaten");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "tsv_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "tsv_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "SchulungException"),
                      "tsv_service.SchulungExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "schulungExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "tsv_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "tsv_service.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getSchulungsszenarien");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(java.lang.String[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "return"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "tsv_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "tsv_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "SchulungException"),
                      "tsv_service.SchulungExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "schulungExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "tsv_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        _operations[2] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("setSchulungsszenario");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "dialogId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "szenarioId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "fachgebietsCode"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "cardReaderId"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(org.apache.axis.encoding.XMLType.AXIS_VOID);
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "InvalidParameterException"),
                      "tsv_service.InvalidParameterExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "invalidParameterExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "AccessException"),
                      "tsv_service.AccessExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "ServiceException"),
                      "tsv_service.ServiceExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "SchulungException"),
                      "tsv_service.SchulungExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "schulungExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "DialogException"),
                      "tsv_service.DialogExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "CardException"),
                      "tsv_service.CardExceptionContent",
                      new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent"), 
                      true
                     ));
        _operations[3] = oper;

    }

    public Tsv_3BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public Tsv_3BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public Tsv_3BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.1");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "accessExceptionContent");
            cachedSerQNames.add(qName);
            cls = tsv_service.AccessExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "baseExceptionContent");
            cachedSerQNames.add(qName);
            cls = tsv_service.BaseExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "cardExceptionContent");
            cachedSerQNames.add(qName);
            cls = tsv_service.CardExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "dialogExceptionContent");
            cachedSerQNames.add(qName);
            cls = tsv_service.DialogExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.base.client.chipkarte.at", "serviceExceptionContent");
            cachedSerQNames.add(qName);
            cls = tsv_service.ServiceExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "invalidParameterExceptionContent");
            cachedSerQNames.add(qName);
            cls = tsv_service.InvalidParameterExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://exceptions.soap.tsv.client.chipkarte.at", "schulungExceptionContent");
            cachedSerQNames.add(qName);
            cls = tsv_service.SchulungExceptionContent.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://soap.base.client.chipkarte.at", "property");
            cachedSerQNames.add(qName);
            cls = tsv_service.Property.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public tsv_service.Property[] checkStatus(java.lang.String dialogId) throws java.rmi.RemoteException, tsv_service.AccessExceptionContent, tsv_service.ServiceExceptionContent, tsv_service.SchulungExceptionContent, tsv_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "checkStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (tsv_service.Property[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (tsv_service.Property[]) org.apache.axis.utils.JavaUtils.convert(_resp, tsv_service.Property[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.AccessExceptionContent) {
              throw (tsv_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.ServiceExceptionContent) {
              throw (tsv_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.SchulungExceptionContent) {
              throw (tsv_service.SchulungExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.DialogExceptionContent) {
              throw (tsv_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void deleteSchulungsdaten(java.lang.String dialogId, java.lang.String cardReaderId) throws java.rmi.RemoteException, tsv_service.AccessExceptionContent, tsv_service.ServiceExceptionContent, tsv_service.SchulungExceptionContent, tsv_service.DialogExceptionContent, tsv_service.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "deleteSchulungsdaten"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.AccessExceptionContent) {
              throw (tsv_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.ServiceExceptionContent) {
              throw (tsv_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.SchulungExceptionContent) {
              throw (tsv_service.SchulungExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.DialogExceptionContent) {
              throw (tsv_service.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.CardExceptionContent) {
              throw (tsv_service.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public java.lang.String[] getSchulungsszenarien(java.lang.String dialogId) throws java.rmi.RemoteException, tsv_service.AccessExceptionContent, tsv_service.ServiceExceptionContent, tsv_service.SchulungExceptionContent, tsv_service.DialogExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "getSchulungsszenarien"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (java.lang.String[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (java.lang.String[]) org.apache.axis.utils.JavaUtils.convert(_resp, java.lang.String[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.AccessExceptionContent) {
              throw (tsv_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.ServiceExceptionContent) {
              throw (tsv_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.SchulungExceptionContent) {
              throw (tsv_service.SchulungExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.DialogExceptionContent) {
              throw (tsv_service.DialogExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public void setSchulungsszenario(java.lang.String dialogId, java.lang.String szenarioId, java.lang.String fachgebietsCode, java.lang.String cardReaderId) throws java.rmi.RemoteException, tsv_service.InvalidParameterExceptionContent, tsv_service.AccessExceptionContent, tsv_service.ServiceExceptionContent, tsv_service.SchulungExceptionContent, tsv_service.DialogExceptionContent, tsv_service.CardExceptionContent {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[3]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "setSchulungsszenario"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {dialogId, szenarioId, fachgebietsCode, cardReaderId});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        extractAttachments(_call);
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.InvalidParameterExceptionContent) {
              throw (tsv_service.InvalidParameterExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.AccessExceptionContent) {
              throw (tsv_service.AccessExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.ServiceExceptionContent) {
              throw (tsv_service.ServiceExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.SchulungExceptionContent) {
              throw (tsv_service.SchulungExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.DialogExceptionContent) {
              throw (tsv_service.DialogExceptionContent) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof tsv_service.CardExceptionContent) {
              throw (tsv_service.CardExceptionContent) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
