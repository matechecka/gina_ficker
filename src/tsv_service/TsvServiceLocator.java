/**
 * TsvServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tsv_service;

public class TsvServiceLocator extends org.apache.axis.client.Service implements tsv_service.TsvService {

    public TsvServiceLocator() {
    }


    public TsvServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public TsvServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for tsv_3
    private java.lang.String tsv_3_address = "http://localhost/tsv/3";

    public java.lang.String gettsv_3Address() {
        return tsv_3_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String tsv_3WSDDServiceName = "tsv_3";

    public java.lang.String gettsv_3WSDDServiceName() {
        return tsv_3WSDDServiceName;
    }

    public void settsv_3WSDDServiceName(java.lang.String name) {
        tsv_3WSDDServiceName = name;
    }

    public tsv_service.ITsvService gettsv_3() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(tsv_3_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return gettsv_3(endpoint);
    }

    public tsv_service.ITsvService gettsv_3(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            tsv_service.Tsv_3BindingStub _stub = new tsv_service.Tsv_3BindingStub(portAddress, this);
            _stub.setPortName(gettsv_3WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void settsv_3EndpointAddress(java.lang.String address) {
        tsv_3_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (tsv_service.ITsvService.class.isAssignableFrom(serviceEndpointInterface)) {
                tsv_service.Tsv_3BindingStub _stub = new tsv_service.Tsv_3BindingStub(new java.net.URL(tsv_3_address), this);
                _stub.setPortName(gettsv_3WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("tsv_3".equals(inputPortName)) {
            return gettsv_3();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "TsvService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://soap.tsv.client.chipkarte.at", "tsv_3"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("tsv_3".equals(portName)) {
            settsv_3EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
