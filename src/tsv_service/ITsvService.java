/**
 * ITsvService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package tsv_service;

public interface ITsvService extends java.rmi.Remote {
    public tsv_service.Property[] checkStatus(java.lang.String dialogId) throws java.rmi.RemoteException, tsv_service.AccessExceptionContent, tsv_service.ServiceExceptionContent, tsv_service.SchulungExceptionContent, tsv_service.DialogExceptionContent;
    public void deleteSchulungsdaten(java.lang.String dialogId, java.lang.String cardReaderId) throws java.rmi.RemoteException, tsv_service.AccessExceptionContent, tsv_service.ServiceExceptionContent, tsv_service.SchulungExceptionContent, tsv_service.DialogExceptionContent, tsv_service.CardExceptionContent;
    public java.lang.String[] getSchulungsszenarien(java.lang.String dialogId) throws java.rmi.RemoteException, tsv_service.AccessExceptionContent, tsv_service.ServiceExceptionContent, tsv_service.SchulungExceptionContent, tsv_service.DialogExceptionContent;
    public void setSchulungsszenario(java.lang.String dialogId, java.lang.String szenarioId, java.lang.String fachgebietsCode, java.lang.String cardReaderId) throws java.rmi.RemoteException, tsv_service.InvalidParameterExceptionContent, tsv_service.AccessExceptionContent, tsv_service.ServiceExceptionContent, tsv_service.SchulungExceptionContent, tsv_service.DialogExceptionContent, tsv_service.CardExceptionContent;
}
